const axios = require('axios')
const getToken = require('../src/generateToken');

describe('Probando que las sesiones se creen correctamente', () => {
    it('Generando sesion', async () => {

        let axiosConfig = {
            headers: {
              'X-OPENTOK-AUTH': getToken(),
              'Accept': 'application/json'
            }
          };
        
        const response = await axios.post('https://api.opentok.com/session/create', '', axiosConfig);
        const data = response.data[0];
        expect(data.session_id.length).toBeGreaterThan(0);
        expect(data).not.toBeNull();
        expect(data).not.toBeUndefined();

    });
});