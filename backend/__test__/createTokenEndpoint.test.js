const request = require('supertest');
const app = require('../src/index');

describe('Probando el endpoint para crear sesiones y token', () => {

    it('Llamando al endpoint', done => {
        request(app)
            .get('/meeting')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });

});