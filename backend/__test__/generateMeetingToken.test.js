const axios = require('axios')
const getToken = require('../src/generateToken');
const getMeetingToken = require('../src/generateMeetingToken');

describe('Probando que el token se cree correctamente', () => {
    it('Generando token', async () => {

        let axiosConfig = {
            headers: {
              'X-OPENTOK-AUTH': getToken(),
              'Accept': 'application/json'
            }
          };
        
        const response = await axios.post('https://api.opentok.com/session/create', '', axiosConfig);
        const session_id = response.data[0].session_id;
        const token = getMeetingToken(session_id);

        expect(token).not.toBeNull();
        expect(token).not.toBeUndefined();
        expect(token.length).toBeGreaterThan(0);
    });
});