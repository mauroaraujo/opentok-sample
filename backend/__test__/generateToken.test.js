const getToken = require('../src/generateToken');

describe('Probando que se generen correctamente los tokens', () => {

    it('Generando token', () => {
        expect(getToken()).not.toBeNull();
        expect(getToken()).not.toBeUndefined();
        expect(getToken().length).toBeGreaterThan(0);
    });

})