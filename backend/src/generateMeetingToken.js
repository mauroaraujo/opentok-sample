require('dotenv').config();

const OpenTok = require('opentok');
const opentok = new OpenTok(process.env.OPENTOK_KEY, process.env.OPENTOK_SECRET_KEY);

const getMeetingToken = (session_id) => {
    return opentok.generateToken(session_id);
}

module.exports = getMeetingToken;