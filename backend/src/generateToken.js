require('dotenv').config();
const jwt = require("jsonwebtoken");

const getToken = () => {
  const secret = process.env.OPENTOK_SECRET_KEY;
  
  const token = jwt.sign(
    {
      iss: process.env.OPENTOK_KEY, // Replace this with your OpenTok API Key on .env
      ist: "project",
      iat: Math.floor(Date.now() / 1000), // e.g. 1472691002
      exp: Math.floor(Date.now() / 1000) + 99999999, // e.g. 1472691302
    },
    secret
  );

  return token;
}

module.exports = getToken;