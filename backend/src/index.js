require('dotenv').config();

const express = require('express');
const app = express();
const axios = require('axios');
const port = process.env.PORT || 8080;

const getToken = require('./generateToken.js');
const getMeetingToken = require('./generateMeetingToken');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get('/meeting', (req, res) => {
  let axiosConfig = {
    headers: {
      'X-OPENTOK-AUTH': getToken(),
      'Accept': 'application/json'
    }
  };

  axios.post('https://api.opentok.com/session/create', '', axiosConfig)
  .then(response => {
    const session_id = response.data[0].session_id;
    const token = getMeetingToken(session_id);

    res.status(200).json({ 
      message: 'Data generated correctly',
      userToken: getToken(),
      session_id: session_id,
      meeting_token: token
    });
  })
  .catch(error => console.log(error));
});

app.listen(port, () => console.log(`listen on port ${port}`));

module.exports = app;